# Nouveau Kernel Module Parameters

For an up-to-date list of parameters, run `modinfo nouveau` in your terminal. Below a slightly more detailed list of some of the parameters. Each of these parameters expects a value. Most are simple booleans, and can be set with `foo=1` for enable or `foo=0` for disable. The rest have detailed explanations of the values they're allowed to take on.

### `perflvl`/`perflvl_wr` (removed in 3.13)

Specify `nouveau.perflvl_wr=7777` in order to be able to switch performance levels at runtime via `/sys/class/drm/cardN/device/performance_level`. You can also specify `nouveau.perflvl=N` to switch to a specific performance level on boot.

### `pstate` (removed in 4.5)

Specify `nouveau.pstate=1` in order to be able to switch performance levels at runtime via `/sys/class/drm/cardN/device/pstate`. Starting in 4.5, this is available by default in debugfs, at `/sys/kernel/debug/dri/*/pstate`

**WARNING**: Power management is a very experimental feature and is not expected to work. If you decided to upclock your GPU, please aknowledge that your card may overheat. Please check the temperature of your GPU at all time!

### `runpm`
Force enable 1 or disable 0, runtime power-managment. The default only for Optimus systems is -1. 

### `noaccel`

Disable kernel/abi16 acceleration (i.e. 1 for disable acceleration, 0 for enable).

### `nofbaccel`

Disable fbcon acceleration (i.e. 1 for disable acceleration, 0 for enable).

### `modeset`

Whether the driver should be enabled. 0 for disabled, 1 for enabled, 2 for headless

### `config`

This provides a way to configure various parts of the driver, it is a comma-separate list of a=b key/value pairs. The values are all integers (use 0/1 for booleans) except for `NvBios` which is a string.

* $engine: Whether to enable/disable the engine, e.g. PDISP=0
* `NvAGP`: The agp mode (0 for disable) to force (starting with 4.3)
* `NvBios`: Specify VBIOS source as one of `OpenFirmware/PRAMIN/PROM/ACPI/PCIROM/PLATFORM`, or a filename passed to request_firmware
* `NvBoost`: Specify the Boost mode for Fermi and newer. 0: base clocks (default) 1: boost clocks 2: max clocks (starting with 4.10)
* `NvClkMode`: Force a particular clock level on boot. Note that this does not parse hex, so for clock mode `f`, pass in `15`.
* `NvClkModeAC`: Same as `NvClkMode`, when the power is plugged in
* `NvClkModeDC`: Same as `NvClkMode`, when running off battery
* `NvFanPWM`: Enable use of PWM for the fan, auto-detect by default
* `NvForcePost`: Whether to force a POST of the device, off by default
* `NvGrUseFW`: Whether to load FW for PGRAPH on NVC0
* `NvI2C`: ??, off by default
* `NvMemExec`: Perform memory reclocking
* `NvMSI`: Use MSI interrupts, on by default on the chipsets that support it
* `NvMXMDCB`: Sanitize DCB outputs from the BIOS, on by default
* `NvPCIE`: NV40 family only, whether to use PCI-E GART, on by default
* `NvPmEnableGating`: Enables clockgating for Kepler GPUs

Here is a list of engines:

* DEVICE
* DMAOBJ
* PBSP
* PCE0
* PCE1
* PCE2
* PCRYPT
* PDISP
* PFIFO
* PGRAPH
* PMPEG
* PPM
* PPPP
* PVP
* SW

### `debug`

This works just like the config option, but the values are all debug levels (one of `fatal, error, warn, info, debug, trace, paranoia, spam`). Note that in order for a debug level to work, `CONFIG_NOUVEAU_DEBUG` must be set to at least that level.

* `CLIENT`
* $subdev (e.g. PDISP=debug)

Here is a list of the available subdevices:

* BARCTL
* DEVINIT
* GPIO
* I2C
* INSTMEM
* MXM
* PBUS
* PIBUS
* PLTCG
* PTHERM
* PTIMER
* VBIOS
* any engine (see above)

Example: nouveau.debug="PTHERM=debug,PTIMER=debug"

### `agpmode` (removed in 4.3)

AGP mode, 0 to disable AGP

### `vram_pushbuf`

Create DMA push buffers in VRAM

### `ignorelid`

Ignore ACPI lid status

### `duallink`

Allow dual-link TMDS (on by default)

### `tv_norm`

Default TV norm. Default is PAL. Valid values: PAL, PAL-M, PAL-N, PAL-Nc, NTSC-M, NTSC-J, hd480i, hd480p, hd576i, hd576p, hd720p, hd1080i.

This only applies to cards that don't have external encoders.

### `tv_disable`

Disable TV-out detection
