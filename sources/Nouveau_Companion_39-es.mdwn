[[!table header="no" class="mointable" data="""
 [[Home|FrontPage]]  |  [[TiNDC 2006|IrcChatLogs]]  |  [[TiNDC 2007|IrcChatLogs]]  |  [[TiNDC 2008|IrcChatLogs]]  |  [[Old Logs|IrcChatLogs]]  |  [[Current Logs|IrcChatLogs]]  | DE/[[EN|Nouveau_Companion_39]]/[[ES|Nouveau_Companion_39-es]]/[[FR|Nouveau_Companion_39-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Informe irregular sobre el desarrollo de Nouveau


## Edición del 29 de abril


### Introducción

Hemos tardado un poco más en elaborar este número ya que el canal ha estado lamentablemente tranquilo en las últimas semanass. En parte debido a que Marcheu se encontraba en una conferencia, y otros desarrolladores estaban a la espera de pistas o código suyo. Por lo que, para tener algo para este número, me dirigí en privado a los desarrolladores, para preguntarles acerca de su trabajo durante las últimas semanas. Tras ello, remontó la actividad pero estaba liado con una actualización del hardware de mi equipo y no tuve tiempo suficiente para seguir el canal. Y cuando intenté ponerme al día, freedesktop.org hace una actualización del sistema... Así que falta al menos el contenido de una semana, pero ya ha llevado demasiado tiempo llegar hasta aquí, por lo que se trata de hacerlo ahora o nunca :). 

Por tanto, este número tocará unos pocos aspectos, y debe consultarse el repositorio en git o los registros del canal para obtener información adicional. 

Airlied informó en su bitácora sobre su trabajo para añadir un adelanto de lo que será el establecimiento de modo de video a través del kernel en FC9. No se trata del resultado final, pero no estará demasiado lejos de lo que se pretende. Así que la integración de ese código en el kernel principal no debería estar tampoco muy lejana. ([[http://airlied.livejournal.com/58778.html|http://airlied.livejournal.com/58778.html]]) 

Y phoronix.com realizó un pequeño test del código en el siguiente artículo: [[http://www.phoronix.com/scan.php?page=article&item=kernel_modesetting&num=1|http://www.phoronix.com/scan.php?page=article&item=kernel_modesetting&num=1]] 

Por cierto, la versión actual de Fedora tiene disponible el paquete Nouveau. Si encuentras algún problema con él, prueba antes con la versión más actual en git antes de informar de fallos. Nouveau está evolucionando muy rápidamente y el paquete enseguida se queda obsoleto. 

Aparte de eso, por favor haced todas las pruebas que podáis (¡usad siempre Randr12!) en informad de los fallos. Estamos muy interesados en vuestros resultados y la experiencia de usuario. 

Y si todavía os preguntáis qué es exactamente Gallium3D, echad un vistazo a este enlace: [[http://jrfonseca.blogspot.com/2008/04/gallium3d-introduction.html|http://jrfonseca.blogspot.com/2008/04/gallium3d-introduction.html]]. 

Dado que la pregunta aparece en el canal IRC unas dos veces al mes: "¿Habrá una versión para *BSD* de Nouveau?", la respuesta es: "Depende". Es necesaria una versión del DRM y el TTM actual para que pueda llegar a funcionar. Más allá de eso, no debería ser necesario realizar muchos cambios para que funcione en cualquier sistema BSD. Así que dad un paso adelante si estáis interesados en hacer esa versión. ¡Queremos saber que estáis en ello!. 


### Estado actual

Tras bastante tiempo, el arreglo de <ins>ucmpdi2 en PPC acabó integrado en el kernel. Esto debería solucionar algunos problemas y preguntas recurrentes sobre la ausencia de este símbolo. 

En estos momentos, la mayor parte del trabajo que se está haciendo está relacionado con Gallium 3D. Como Marcheu ha estado ausente durante un tiempo, el trabajo en las tarjetas más antiguas que NV4x se ha detenido algo, ya que él pretendía escribir código para tarjetas NV2x y anteriores. Sin embargo, p0g ha sido capaz de visualizar glxgears en NV1x. Además, parte del código de NV1x de Marcheu ha acabado en git: 

* [[http://gitweb.freedesktop.org/?p=nouveau/mesa.git;a=commit;h=8f26e975ca6341cb3366a18beb352b5cdcaee2bc|http://gitweb.freedesktop.org/?p=nouveau/mesa.git;a=commit;h=8f26e975ca6341cb3366a18beb352b5cdcaee2bc]] hasta [[http://gitweb.freedesktop.org/?p=nouveau/mesa.git;a=commit;h=8ed894bd17bd6f426a0d87f7113f23043cda3bc3|http://gitweb.freedesktop.org/?p=nouveau/mesa.git;a=commit;h=8ed894bd17bd6f426a0d87f7113f23043cda3bc3]] 
pmdata también tuvo que trabajar en otros proyectos, pero, sin embargo, fue capaz de añadir un par de parches al código de las NV3x. [[http://gitweb.freedesktop.org/?p=nouveau/mesa.git;a=commit;h=7b389f8d2f307fa0714494f2a43e9141cc04ed3e|http://gitweb.freedesktop.org/?p=nouveau/mesa.git;a=commit;h=7b389f8d2f307fa0714494f2a43e9141cc04ed3e]] 

Bien, parece que esta vez dimos en el clavo con el Summer of Code de Google. Aunque siempre habíamos llegado tarde, al final logramos reclutar a ymanton. Trabajará en la decodificación de video haciendo uso de instrucciones de sombreado. Primero hará una implementación prototipo usando softpipe de Gallium3D, y luego lo reimplementará para Nouveau. Por favor, tened en cuenta que esta interfaz será genérica, por lo que otros controladores Gallium serán capaces de usarla como referencia. Para comprobar si la idea es factible, Nouveau tendrá una implementación usando el hardware de la tarjeta (mientras que softpipe es únicamente a través de software). 

Pero, hablemos un poco sobre el establecimiento de modo en las tarjetas NV5x. Como sabéis, tenemos algunos problemas en ese campo. El hardware es completamente distinto al que se había usado hasta las NV4x. 

Stillunknown lo ha intentado una y otra vez, logrando casi siempre resultados improductivos. Finalmente, se dio cuenta de que el controlador binario leía algunos registros que nunca se inicializaban o se escribían. Comprobó con pq si él o MMio había cometido algún error al hacer el trazado MMio, pero eso parecía improbable. 

Así que la cuestión estaba sobre la mesa: ¿cómo se fija el modo?. La sospecha se centraba en la posibilidad de que se hiciese a través de una FIFO reservada. La idea estaba basada en el hecho de que Darktama era todavía incapaz de hacer funcionar la FIFO 0 en las tarjetas NV5x. 

Así que, de forma independiente, llegó a una conclusión similar: la FIFO 0 se usaba para el establecimiento de modo. ([[http://people.freedesktop.org/~marcheu/irclogs/nouveau-2008-04-27#1955|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2008-04-27#1955]]) 

Con esa idea en mente, stillunknown rehizo el código de establecimiento de modo para las NV5x durante sus vacaciones, y por ello estuvo poco tiempo en el canal. Envió todos los cambios al repositorio y pidió a KoalaBR que probase el nuevo código en una 8600GT. Incapaz de encontrar una buena excusa para no hacerlo, KoalaBR se sintió obligado a hacerlo, e informó de su éxito. Todo lo que funcionaba con el código anterior también lo hace con el nuevo. Sin embargo, el cambio a modo de texto no es todavía posible. 

A propósito del establecimiento de modo, tuvimos problemas con las tarjetas NV2x y NV3x que usaban DVI. Se debía a que esas tarjetas incluyen chips DVI externos, mientras que las tarjetas más modernas incluyen esa funcionalidad en el chip principal. Malc0 solucionó algunas cosillas y al fin comenzaron a funcionar algunas tarjetas NV2x/NV3x ccon DVI. 

Así que, justo antes de finalizar mi borrador, malc0 cambió el código para que funcione con Randr1.2 por defecto. Por favor, probadlo e informad de los problemas que podáis encontrar con el cambio de modo y probad también si el código anterior tiene el mismo problema o no (desactivando explícitamente [[RandR1.2|Randr12]]). 

Y ya que hablamos de Darktama, su trabajo en Gallium3D tampoco se ha detenido. El trabajo habitual es seguir el progreso de Gallium en su repositorio principal. Ello implica integrar los cambios, resolver los conflictos, y modificar nuestra interfaz de acuerdo a los cambios que se hayan producido. 

Además, Darktama ha añadido una interfaz TTM muy básica que casi funciona, aunque todavía es demasiado lenta. La razón es que la gestión del buffer es extremadamente simple por ahora. 

Y, ¿por qué es lento ese código?, ¿se debe a problemas genéricos en el código TTM o en nuestro código?. Veamos lo que Darktama tiene que decir al respecto: "El problema está en mi código del buffer para gallium. Lo copié y modifiqué muy rápidamente para que funcionase con el TTM, por lo que perdí un montón de trabajo previo de optimización, parte del cual es bastante crítico de cara al rendimiento :)" 

Y, sí, y los problemas con los reflejos en Neverball también se han resuelto. Primero, Darktama y el equipo de Gallium pensaron que habían pasado por alto algún detalle importante de diseño, que hacía los reflejos complicados y farragosos de implementar. Simplemente habían pasado por alto una opción, y todo resultó más sencillo de implementar. 

Bien, el periodo de integración de cambios del kernel 2.6.26 ha pasado y MMio no logró entrar. No se debió a falta de trabajo en MMioTrace por parte de pq (echad un vistazo a su página de MMioTrace en el wiki, donde actualiza continuamente el estado actual), sino a que el gran número de cambios que la inclusión de ftrace requería hizo que se pospusiese su integración hasta el kernel 2.6.27. Una vez que ftrace sea integrado podremos ver la brillante implementación de MMioTrace que ha hecho pq, así que estad atentos. 

Y jb17some continuó su trabajo con XvMC. Su biblioteca de decodificación parece funcionar para NV4x, aunque todavía existen algunos detalles y trucos que hay que pulir. La biblioteca se ha importado al CVS de sourceforge y se puede conocer el estado actual en todo momento en este enlace: [[http://nouveau.freedesktop.org/wiki/jb17bsome|http://nouveau.freedesktop.org/wiki/jb17bsome]] CVS:http://nouveau.cvs.sourceforge.net/nouveau/nv40_xvmc/ 

Y, finalmente, al tercer intento, conseguimos un puesto en el Summer of Code de Google a través de Xorg. Ymanton está trabajando en la creación de una infraestructura de decodificación de vídeo sobre Gallium3D. Se implementará una versión de referencia usando softpipe (visualizador software de Gallium3D) y al final la funcionalidad de implementará específicamente para Nouveau. Así que ¡damos una calurosa bienvenida y deseamos mucha suerte a ymanton!. Su bitácora para el proyecto está aquí: [[http://www.bitblit.org/gsoc/gallium3d_xvmc.shtml|http://www.bitblit.org/gsoc/gallium3d_xvmc.shtml]] 


### Ayuda necesaria

Todavía necesitamos saber qué tal funciona el nuevo código Randr1.2. Como mencionamos antes, ahora es el camino por defecto. Por favor, probadlo e informad de los resultados a stillunknown o malc0. 
