## The irregular Nouveau Development Companion


## Issue #3 for September, 10th


### Introduction

Thanks for once again reading our TiNDC, still regular though, as I am still waiting for xorg 7.1  to go stable on Gentoo. Bad for me, good for you as we see yet another issue TiNDC. The last issue saw the Wiki treatment, that's how it should be (especially with regards to those stupid typos, sorry but english is not my native language). 

Criticism, ideas and questions can be send in my (KoalaBR) direction on the #nouveau IRC channel, I will try to address them in the next TiNDC. 


### Status

The start of the week saw only very few actions by the lead developers on the #nouveau channel, which in turn was unusually quiet. But pmdata came to the rescue with some additions and corrections to  renouveau and the accompanying documentation, while darktama and marcheu did have a deep look at the setup of the cards. While marcheu did look at the interrupt system, notifier possibilities and finally memory setup (what is available to user space and when), darktama tried to setup the card via the binary Nvidia blob and  let his nouveau version render 3D primitives via the EXA hooks. **Correction**, just to avoid missunderstandings: The card was initialised via the nvidia blob, which was then rmmod'ded and nouveau.ko was inserted. So nouveau did the rendering without any further help from the nvidia blob. 

First version had quite some visual bugs, but it used the 3d engine for 2D acceleration. After correcting this,  we saw some happy  dancing, when the color rendering worked, followed immediately by dispair after realising, that after any soft reboot of the card, the EXA code didn't work anymore (Only a fresh insmod of the blob resulted in "correct" rendering). Again, bugs were squashed and on tuesday, darktamas version of nouveau is working  correct, however there are still quite some parts of the screen which are software rendered (normally those which are correctly displayed). 

Correct rendering was the other problem, which was noted: Moving a window around yielded "melting" windows.  More and more of the window was simple not rendered the longer the window was moved without releasing the mouse button. It was found, that this was due to the combination of EXA and nouveau using the same memory for source and destination for rendering. 

Marcheu and Darktama discussed the further development of the internal design, especially whether 2D should be on top of the 3D engine, which would lead to simple (and hardware accelerated) rotation on the desktop. So in order to avoid code multiplication (e.g. "EXA_hookNV4(), EXA_hookNV15(), EXA_hookG70()....) an abstraction layer ("miniGL") for leveling the different card features would be a logical consequence. Something like "draw_quad" and "set_alpha_func" stuff, implement them for everything from NV04 to  NV40 with the 3D engine, and then stack them up as we want to achieve EXA. Disadvantages: 

1. The card doesn't like having a texture as the rendering surface at the same time (EXACopy) - so we'd have to do blits to temporary memory 

2. Currently nobody knows how to implement planemask with the 3D engine (though, this X11 fallback doesn't seem to be used anywhere) 

Finally KoalaBR submitted a patch to autogenerate nouveau_reg.h from objects.c in nouveau. This still needs  some testing in the generated defines for matrices, but mainly should be working ok. 


### Help needed

We still are looking for C developers who aren't afraid to get their hands dirty. Writing of the driver could benefit from developers, who already know their way around graphics drivers or aren't afraid to learn. 

Further, Pmdata is still looking for quadro dumps, especially for stereo dumps. The age of the card  doesn't matter much, please contact him, if you can help. 

[[<<< Previous Issue|NouveauCompanion_2]] | [[Next Issue >>>|NouveauCompanion_4]] 
