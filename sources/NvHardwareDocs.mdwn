We collect NV video card hardware information here. 

* [[envytools|https://github.com/envytools/envytools]]: THE repository of reverse engineered documentation and RE tools 
* [[Block Architecture Diagram|https://web.archive.org/web/20080111122628/http://www.digit-life.com/articles2/gffx/nv40-part1-a.html]] for the GeForce series 
* [[block diagrams|http://freenv.svn.sourceforge.net/viewvc/freenv/doc/shaderinsnformat/bitdiagen/]] for the nouveau series 40 instruction format 
* The [[the freenv|http://sourceforge.net/projects/freenv/]] project has some techical code in it's SVN (only for experts, not much useable) 
* [[Shading FAQ|http://web.archive.org/web/20090123215411/http://developer.nvidia.com/object/geforce3_faq.html]] for GeForce 
* [[Texture formats|http://web.archive.org/web/20090123215411/http://developer.nvidia.com/object/nv_ogl_texture_formats.html]] supported by NVidia GPUs. 
* [[Guard band clipping|http://web.archive.org/web/20090123215411/http://developer.nvidia.com/object/Guard_Band_Clipping.html]] used to clip rendering output to viewport. 
* names given to 3D objects on different cards :  
   * nv10 - celsius 
   * nv20 - kelvin 
   * nv30 - rankine 
   * nv40 - curie 
   * nv50 - tesla 
* [[RivaTV (particularly see the "rules" file)|http://rivatv.sourceforge.net/devs.html]] 
* [[Nvidia bioses for TNT2 -> Geforce 4|http://www.x86-secret.com/articles/nvbios.htm]] 
* [[More Nvidia bioses|http://www.station-drivers.com/page/vgabios.htm]] 
* [[Output load detection on nvidia hardware|Load_Detection]] 
* [[Surface layout details|Surface_Layouts]] 

### Vertex Shading info

* [[http://icps.u-strasbg.fr/~marchesin/perso/extensions/NV/vertex_program2.html|http://web.archive.org/web/20080122101253/http://icps.u-strasbg.fr/~marchesin/perso/extensions/NV/vertex_program2.html]] Explanation of NV_vertex_program2 vertex shading extension 
* [[http://www.pny.com/support/downloads/vertoTech/CineFXShaders.pdf|http://web.archive.org/web/20070307000610/http://www.pny.com/support/downloads/vertoTech/CineFXShaders.pdf]] Page 7 has a list of all vertex shading instructions it seems 

### Nvidia whitepapers

Architecture specific whitepapers:

* [[Fermi|https://www.nvidia.com/content/PDF/fermi_white_papers/P.Glaskowsky_NVIDIA%27s_Fermi-The_First_Complete_GPU_Architecture.pdf]]
* [[Another Fermi|https://www.nvidia.com/content/PDF/fermi_white_papers/NVIDIA_Fermi_Compute_Architecture_Whitepaper.pdf]]
* [[Kepler|https://www.nvidia.com/content/dam/en-zz/Solutions/Data-Center/tesla-product-literature/NVIDIA-Kepler-GK110-GK210-Architecture-Whitepaper.pdf]]
* [[Maxwell|https://www.microway.com/download/whitepaper/NVIDIA_Maxwell_GM204_Architecture_Whitepaper.pdf]]
* [[Pascal|https://images.nvidia.com/content/pdf/tesla/whitepaper/pascal-architecture-whitepaper.pdf]]
* [[Volta|https://images.nvidia.com/content/volta-architecture/pdf/volta-architecture-whitepaper.pdf]]
* [[Turing|https://www.nvidia.com/content/dam/en-zz/Solutions/design-visualization/technologies/turing-architecture/NVIDIA-Turing-Architecture-Whitepaper.pdf]]
* [[Ampere|https://www.nvidia.com/content/dam/en-zz/Solutions/geforce/ampere/pdf/NVIDIA-ampere-GA102-GPU-Architecture-Whitepaper-V1.pdf]]

Card specific documentations and whitepapers:

* [[GTX 200 technical breif|https://www.nvidia.com/docs/IO/55506/GeForce_GTX_200_GPU_Technical_Brief.pdf]]
* [[GeForce 6 GPU architecture|https://download.nvidia.com/developer/GPU_Gems_2/GPU_Gems2_ch30.pdf]]
* [[GTX 750TI whitepaper|https://www.techpowerup.com/gpu-specs/docs/nvidia-gtx-750-ti.pdf]]
* [[GF100 whitepaper|https://www.ece.lsu.edu/gp/refs/gf100-whitepaper.pdf]]
* [[GTX 1080 whitepaper|http://www.es.ele.tue.nl/~heco/courses/ECA/GPU-papers/GeForce_GTX_1080_Whitepaper_FINAL.pdf]]

### Want to know more about Nvidia chips?

* [[The nVidia card range|http://en.wikipedia.org/wiki/Comparison_of_NVIDIA_Graphics_Processing_Units]] in Wikipedia 
* [[Review|CardReviews]] links 

### General, not related to NV cards

* [[Know your AGP card|http://www.playtool.com/pages/agpcompat/agp.html]] 
