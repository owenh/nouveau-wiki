## Enabling Randr12 support in nouveau

Randr 1.2 is used by Nouveau. No need to explicitly enable it, and you cannot disable it. 


## Generic HowTos

* Simple step-by-step guide to create a very clean Randr12-based config file with all old junk removed: 
   * [[http://wiki.debian.org/XStrikeForce/HowToRandR12|http://wiki.debian.org/XStrikeForce/HowToRandR12]] 
* Very nice illustrations but lots of examples which you'll not apply to Nouveau, like TV output support: 
   * [[http://www.thinkwiki.org/wiki/Xorg_RandR_1.2|http://www.thinkwiki.org/wiki/Xorg_RandR_1.2]] 

## Dual Head setup


### xorg.conf


#### Sections and configuration settings to remove

Randr 1.2 does not use multiple Screen sections, so if you have those you have to remove them from your xorg.conf : 

* If you have two sections like these, remove one of these sections (in this example Screen1):

        Section "Screen"
            Identifier     "Screen0"
        ...

        Section "Screen"               <==== Remove this second section, if you have sone (start removing lines here)
            Identifier     "Screen1"
        ...
        EndSection                     (stop removing lines here)

* If you have a Option "MonitorLayout" or/and "Screen" options in the Device section, remove them 
* Also remove the second Screen entry from the ServerLayout section. Example :

        Screen      0  "Screen0" 1680 0
        Screen      1  "Screen1" LeftOf "Screen0" <= remove this one (for Screen1)

* Also remove any RightOf/LeftOf indications from the remaining Screen line in the ServerLayout section (see above) 

#### Settings on xorg.conf for Randr12

Note that with today's software you often don't need a xorg.conf file at all for nouveau and dual-head to work. 

If you do not preconfigure a left/right/above/below setup in your xorg.xonf (see next section) add `Virtual x y` to your Display Section: 
[[!format txt """
Section "Screen"
  Identifier   "Your Screen identifier here"
  DefaultDepth 24
  SubSection "Display"
    Depth      24
    Virtual 3360 1050
  EndSubSection
  Device       "Your Device identifier here"
EndSection
"""]]
The example above configures a framebuffer for two monitors, each using resolutions up to 1680x1050 pixels, put side-by-side dynamically, after you started X. The values are calculated like this: 

    Virtual <width of the two monitors, e.g. 1680+1680=3360> <maxium height to used by the montiors, e.g. 1050>

Only the part which the monitors show is used: You can start and use X with one monitor as ususal but later add a new monitor and use `xrandr` with `--leftof` or `--rightof` to place the new monitor left or right of your primary monitor.
 
#### Preconfigured side-by-side (LeftOf/RightOf) setup thru xorg.conf

If you have done the above, you can also preconfigure the two monitors to be side-by-side right from the X startup. With this, you do not even need the "Virtual <x> <y>" line because then, the X server already knows before starting that it has to allocate a virtual framebuffer which fits both Monitors side-by-side. 

You need to make the following changes for this to work (simplest way): 

* Change the Monitor identifiers in your xorg.conf to the identifiers which "xrandr" reports and add a LeftOf/RightOf to one of the Monitor sections. Example:

        Section "Monitor"
          Identifier   "DVI-D-0"
          ...
        Section "Monitor"
          Identifier   "VGA-0"
          ...
          Option "LeftOf" "DVI-D-0"

Then restart X and enjoy :-) 
## Examples for the usage of the Xorg xrandr tool to manipulate the monitor setup

Usage: `xrandr -h` gives a short list of available options, `man xrandr` shows you the xrandr manual. 

When Randr12 is enabled, you can check the current Randr12 configuration with with `xrandr -q` (or no options): 
[[!format txt """
xrandr -q
Screen 0: minimum 320 x 200, current 2960 x 1050, maximum 3000 x 1050
DVI-A-0 disconnected (normal left inverted right x axis y axis)
DVI-D-0 connected 1680x1050+0+0 (normal left inverted right x axis y axis) 474mm x 296mm
   1680x1050      60.0*+
   1280x1024      75.0     59.9
   1280x960       59.9
   1152x864       75.0     74.8
   1024x768       75.1     70.1     60.0
   832x624        74.6
   800x600        72.2     75.0     60.3     56.2
   640x480        75.0     72.8     66.7     60.0
   720x400        70.1
VGA-0 connected 1280x1024+1680+0 (normal left inverted right x axis y axis) 350mm x 262mm
   1280x1024      84.8*    75.0
   1152x864       75.0
   1024x768       84.9     75.1     70.1     60.0     59.9     43.5
   832x624        74.6
   800x600        84.9     72.2     75.0     60.3     56.2
   640x480        84.6     75.0     72.8     74.8     66.7     60.0
   720x405        69.6
   720x400        87.8     70.1
"""]]
Change the mode of an output using: 
[[!format txt """
xrandr --output <output>  --mode <mode to switch to>
"""]]
To turn a newly connected monitor on, use 
[[!format txt """
xrandr --output <display>  --auto
"""]]
 or 
[[!format txt """
xrandr --output <output>  --mode ...x...
"""]]
To turn an output off: 
[[!format txt """
xrandr --output <output>  --off
"""]]
To make an output show the same screen area as an other output (cloned mode, default with Randr12): 
[[!format txt """
xrandr --output <output to change> --same-as <output you want to clone>
"""]]
To put the display of one output left, right, above or below to the display of an existing output use _--left-of, --right-of, --above, --below_ : 
[[!format txt """
xrandr --output <output to change> [ --left-of |--right-of | --above | --below ] <output which shall keep its position>
"""]]
More xrandr examples are found in the generic Randr12 howtos mentioned on the top of this page. 
