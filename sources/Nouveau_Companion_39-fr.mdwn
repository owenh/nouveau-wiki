[[!table header="no" class="mointable" data="""
 [[Accueil|FrontPage-fr]]  |  [[TiNDC 2006|IrcChatLogs-fr]]  |  [[TiNDC 2007|IrcChatLogs-fr]]  |  [[TiNDC 2008|IrcChatLogs-fr]]  |  [[Archives anciennes|IrcChatLogs-fr]]  |  [[Archives récentes|IrcChatLogs-fr]]  | DE/[[EN|Nouveau_Companion_39]]/[[ES|Nouveau_Companion_39-es]]/[[FR|Nouveau_Companion_39-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Le compagnon irrégulier du développement de Nouveau


## Édition du 17 mai 2008


### Introduction

Cette édition a été un peu plus longue que d'habitude à venir, le canal IRC étant, de façon angoissante, plutôt silencieux ces dernieres semaines. En partie parce que Marcheu étaient à une conférence et que d'autres développeurs attendait des informations de lui. Résultat, afin d'avoir quelques informations sur leur travaux de ces dernières semaines, j'ai du importuner les développeurs en privé. 

Et puis, soudainement, le rythme s'est de nouveau intensifié alors que j'étais en plein milieu d'une mise à jour matérielle, et donc que je n'avais plus le temps de suivre le canal. Et quand j'ai voulu rattraper mon retard, freedesktop a lui aussi fait une mise à jour... Donc il y a au moins une semaine manquante, mais comme il a fallu déjà trop longtemps pour en arriver là, c'est maintenant ou jamais :). Cette édition va seulement couvrir certains sujets, intéresser vous aux logs du dépôt Git ou du canal IRC si vous voulez plus d'informations. 

Sur son blog, Airlied a annoncé qu'il avait ajouté une préversion de la gestion des modes dans le noyau sur Fedora 9. Ce n'est pas la version finale mais elle ne devrait pas en être trop loin, et cela augure d'une intégration prochaine dans la branche principale.([[http://airlied.livejournal.com/58778.html|http://airlied.livejournal.com/58778.html]]) 

Phoronix.com en a réalisé un test rapide dans cet article : [[http://www.phoronix.com/scan.php?page=article&item=kernel_modesetting&num=1|http://www.phoronix.com/scan.php?page=article&item=kernel_modesetting&num=1]] 

En passant, Fedora a un paquet avec notre pilote, mais il est probablement déjà un peu vieux, Nouveau évoluant assez rapidement. Bref, si vous avez un problème, commencez par tester la version actuelle (git) avant de rapporter un bug. 

En dehors de ça, testez (avec Randr1.2 !) autant que possible et remontez les bugs. Nous sommes toujours intéressés par vos résultats. Si vous vous demandez toujours ce qu'est exactement Gallium3D, un bon article est disponible ici : [[http://jrfonseca.blogspot.com/2008/04/gallium3d-introduction.html|http://jrfonseca.blogspot.com/2008/04/gallium3d-introduction.html]]. 

Et comme la question réapparaît régulièrement (2x par mois environ) : « est ce qu'il y a un portage de Nouveau sur *BSD ? ». La réponse est : « ça dépend ».  Un portage du DRM actuel et du TTM est nécessaire. En dehors de ça, il ne devrait pas trop nécessiter de travail pour fonctionner sur BSD. SI vous êtes intéressé par le portage, n'hésitez pas à venir en discuter. 


### Statut actuel

Après pas mal de temps, la solution pour le problème de <ins>ucmpdi2 sur PPC a finalement été intégrée au noyau. Cela devrait résoudre les problèmes récurrents de symboles manquants. 

Actuellement, la majorité des développements est faite sur Gallium 3D. Marcheu étant plutôt absent ces derniers temps, le travail sur les cartes < NV4x a ralenti, vu qu'il devait écrire certaines parties du code pour les NV2x et inférieures. 

Néanmoins, p0g a réussi à faire fonctionner glxgears sur NV1x. Et au moins une partie du travail de Marcheu a atterri dans l'arbre des sources. De [[http://gitweb.freedesktop.org/?p=nouveau/mesa.git;a=commit;h=8f26e975ca6341cb3366a18beb352b5cdcaee2bc|http://gitweb.freedesktop.org/?p=nouveau/mesa.git;a=commit;h=8f26e975ca6341cb3366a18beb352b5cdcaee2bc]] à [[http://gitweb.freedesktop.org/?p=nouveau/mesa.git;a=commit;h=8ed894bd17bd6f426a0d87f7113f23043cda3bc3|http://gitweb.freedesktop.org/?p=nouveau/mesa.git;a=commit;h=8ed894bd17bd6f426a0d87f7113f23043cda3bc3]] 

pmdata est occupé avec d'autres projets mais il a tout de même écrit quelques patchs pour les NV3x : [[http://gitweb.freedesktop.org/?p=nouveau/mesa.git;a=commit;h=7b389f8d2f307fa0714494f2a43e9141cc04ed3e|http://gitweb.freedesktop.org/?p=nouveau/mesa.git;a=commit;h=7b389f8d2f307fa0714494f2a43e9141cc04ed3e]] 

Ok, cette fois-ci, il semble que nous soyons arrivé à quelque chose avec le GSOC. Bien que classiquement en retard, nous avons pu recruter ymanton qui va travailler sur l'accélaration du décodage vidéo à l'aide des shaders. Un prototype sera développé avec Gallium3D en utilisant le pilote softpipe avant l'implémentation sur Nouveau. Cette implémentation de référnce sera générique, ce qui permettra aux autres pilotes Gallium3D de l'utiliser. Pour vérifier si l'idée est bonne, l'implémentation Nouveau sera faite de façon matériel (softpipe étant complètement logiciel). Accueillons donc le chaleureusement et souhaitons lui bonne chance ! Son blog est accessible ici : [[http://www.bitblit.org/gsoc/gallium3d_xvmc.shtml|http://www.bitblit.org/gsoc/gallium3d_xvmc.shtml]] 

Parlons un peu de la gestion des modes sur les cartes NV5x, avec lesquels nous avions quelques problèmes, le matériel étant totalement différent des précédents. 

Stillunknown a essayé encore et encore sans parvenir à quelque chose d'utilisable. Il fini par remarquer que le blob lisait certains registres qui n'étaient jamais écrit. Il vérifia ensuite avec pq si lui ou MMioTrace n'avait pas fait d'erreur, ce qui semblait peu probable. 

La question restait donc en suspend, comment les modes étaient ils définis ? Rapidement l'idée se fit jour de l'utilisation d'une FIFO réservée pour la gestion des modes. Confortée par le fait que Darktama était incapable d'utiliser la FIFO 0 sur les NV5x. 

Ainsi, indépendamment, il arriva à la même conclusion : la FIFO 0 est utilisée pour la gestion des modes. ([[http://people.freedesktop.org/~marcheu/irclogs/nouveau-2008-04-27#1955|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2008-04-27#1955]]) 

Poussant l'idée un peu plus loin, stillunknown a complètement réécrit la gestion des modes des NV5x durant des vacances et fut rarement vu sur IRC. Il commita le code en une fois et demanda à KoalaBR de tester avec sa 8600GT. Étant incapable de trouver une bonne excuse pour ne pas le faire, ce dernier testa avec succès ce nouveau code. Tout ce qui fonctionnait avec l'ancien code fonctionne avec le nouveau. Néanmoins, la bascule vers le mode texte n'est toujours pas possible. 

En parlant de gestion des modes, nous avans eu un problème avec les NV2/NV3x et le DVI, du au fait que certaines de ces cartes utilisent une puce externe pour le gérer, celui-ci étant intégré sur les cartes récentes. Malc0 a corrigé quelques problèmes et une partie des cartes s'est mise à fonctionner. 

Juste avant que je termine la première version, malc0 a activé randr1.2 par défaut. Vous êtes donc invité à tester et à remonter tout problème après avoir vérifié si l'ancien code l'a aussi (en désactivant explicitement randr1.2). 

En ce qui concerne Darktama, son travail n'a pas ralenti sur Gallium3D. Le travail de base, qui consiste à suivre les progrès de Gallium. Cela signifie mettre à jour, résoudre les conflits subséquents et adapter les interfaces pour s'adapter aux changements. 

Darktama a également ajouté une interface basique vers le TTM, elle est partiellement fonctionnelle mais très lente actuellement en raison d'une gestion simpliste des tampons. 

Est ce que le code est lent à cause de problème avec le TTM ou est ce spécifique à Nouveau ? Voyons voir ce que Darktama a à dire : « C'est spécifique à mon code pour les tampons, j'ai rapidement remplacé l'existant par un adapté au TTM. Il reste énormément d'optimisation à faire, certaines très critiques pour les performances :). » Ah et oui, le problème avec les réflexions dans Neverball a été résolu. Dans un premier temps, Darktama et l'équipe de Gallium ont pensé qu'il manquait un point important de design, rendant l'implémentation complexe. En réalité, ils avaient simplement oublié une option, et finalement ce fut simple à implémenter. 

La fenêtre d'inclusion est fermée pour le noyau 2,6,26 et Mmiotrace n'y est pas. Cela est du au grand volume de code code inclus qui a différé l'inclusion de ftrace qu'à un manque de travail de pq (voir la page sur [[MmioTrace|MmioTrace]] dans le wiki). Quand ftrace entrera dans le noyau, Mmiotrace suivra rapidement. 

Jb17some a continué son travail sur XvMC. Sa bibliothèque de décodage semble fonctionner pour les NV4x mais il reste quelques détails à améliorer. La bibliothèque a été importer dans le CVS de sourceforge :  [[http://nouveau.freedesktop.org/wiki/jb17bsome|http://nouveau.freedesktop.org/wiki/jb17bsome]] CVS : [[http://nouveau.cvs.sourceforge.net/nouveau/nv40_xvmc/|http://nouveau.cvs.sourceforge.net/nouveau/nv40_xvmc/]] 


### Nous aider

Nous avons toujours besoin de retour sur le code de Randr1.2 même si il est maintenant activé par défaut. Testez et reportez le résultat à Stillunknown ou à Malc0. 

[[<<Édition précédente|Nouveau_Companion_38-fr]] 
