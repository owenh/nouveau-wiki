[[!table header="no" class="mointable" data="""
 [[Home|FrontPage]]  |  [[TiNDC 2006|IrcChatLogs]]  |  [[TiNDC 2007|IrcChatLogs]]  |  [[Old Logs|IrcChatLogs]]  |  [[Current Logs|IrcChatLogs]]  | DE/[[EN|Nouveau_Companion_24]]/[[ES|Nouveau_Companion_24-es]]/FR/RU/[[Team|Translation_Team]] 
"""]]


## Informe irregular sobre el Desarrollo de Nouveau


## Edición del 21 de julio


### Introducción

Hola de nuevo. Este es el número 24 de nuestro TiNDC. En estos momentos el ritmo de desarrollo está aumentando con ahuillet, darktama, pmdata, pq e incluso marcheu apareciendo graciosamente más y más a menudo (afirma que reaparecerá completamente en unas 1 o 2 semanas). 

Si se mantiene el ritmo, estoy plantándome renombrar el TiNDC a "El Informe diario sobre el desarrollo de nouveau" :) 

Mejor así que tener que rebuscar noticias. En este punto, querría agrecederos de nuevo vuestro interés por nuestro proyecto. Sólo 36 horas después de que se publicase el último número, se registraron 1750 accesos, y, 24 horas más tarde, eran aproximadamente 2650. 10 días después ¡estamos en torno a los 5200 accesos!. 


### Estado actual

Darktama publicó sus cambios en el DRM y DDX (rama nv50-branch) para poder disponer de soporte incial para G8x. Ya que la gestión de objetos de las tarjetas 8x00 usa ahora direcciones de 64 bits en lugar de 32 bit (y por tanto tamaños mayores de objetos), posee una mejor abstracción y separación entre los contextos OpenGL, con 64 contextos/FIFOs diferentes disponibles,  y la GPU es ahora capaz de proteger la VRAM de acceso desde la CPU, era necesario realizar algunos cambios. 

De todos modos, antes de que Darktama integrase sus cambios, probó la idea de un "modo de compatibilidad", que hace trabajar las G8x usando objetos e instrucciones de las NV4x. Desgraciadamente no funcionó, y la tarjeta protestaba llamativamente. 

Por lo que el DRM necesitaba ser capaz de manejar los nuevos objetos, lo que se incluyó en los parches de Darktama. Las pruebas en otra G84 tuvieron éxito, de forma que la visualización 2D estaba usando aceleración a través de las rutinas EXA copy y EXA solid. Por favor, usad "MigrationHeuristic greedy" y preparaos para encontraros con los problemas de cambio de consola que ya se mencionaron en números anteriores, al volver a la consola de texto. 

Desgraciadamente, esos parches no se llevaban bien con los de Ahuillet para el DRM, que todavía no habían sido integrados en ese momento. Por lo que éste tuvo que aprender a gestionar conflictos en git a las bravas. 

El próximo asunto en la lista de tareas pendientes de Darktama es la desofuscación de la inicialización de PRAMIN para las NV5x / G8x. Afirma entender ahora la funcionalidad y quiere mejorarla con nuevos parches. El primero de ellos ya se ha integrado y vendrán más en el futuro. 

Siguiendo con los problemas de DMA de Ahuillet, stillunknown ayudó mucho probando numerosas combinaciones de distintas versiones de DRM y DDX. Primero, ahuillet logró hacer funcionar PCIGART en nouveau, pero los resultados de las pruebas obtuvieron distintos niveles de "éxito". Desde cuelgues de DMA (NV43, PCIe, CPU 64 bit), a una EXA acelerada (para tarjetas < NV50), a un Xv más lento (DMA comparado con un simple memcpy(), lo cual no tiene demasiado sentido), todo era posible. 

La confusión se extendión entre las filas de desarrolladores y probadores, pero las altas instancias del proyecto X vinieron al rescate,  apuntando a algunas inconsistencias en la programación del DRM (como el uso de virt_to_bus(), que es una mala idea en PPC o x86_64). IDR llevó a cabo algún trabajo de limpieza, que permitió la supervivencia de los PPC en la inicialización de las X, pero tuvo que abandonar debido a limitaciones de tiempo. 

Benh retomó el trabajo donde IDR lo dejó. Tras dos días de trastear a tiempo parcial, logró dejar el código para PPC en un estado razonable de funcionamiento. Todavía persisten algunos problemas de visualización (fuentes mal dibujadas) y un gestor de ventanas con composición produce errorer de blitting al intentar mostrar sombras. (Hilo: [[http://lists.freedesktop.org/archives/nouveau/2007-July/000200.html|http://lists.freedesktop.org/archives/nouveau/2007-July/000200.html]]). 

Las pruebas preliminares mostraron que el DMA (PCI o AGP) aceleraba enormemente EXA, pero que resultaba más lento para Xv, ya que tomaba más tiempo de CPU y real el mostrar un frame de video. 

La siguiente tarea acometida por ahuillet, jb17some y p0g fue realizar un benchmark de Xv para localizar los cuellos de botella y, tal vez, mejorar el DMA para Xv. No importa demasiado si el DMA de Xv es un poco más lento que la copia de CPU, puesto que libera la CPU para otras tareas, por ejemplo, descodificar el siguiente frame de video.  

jb17some colaboró usando oprofile con nouveau_drv.so en esos casos. Mostró que la mayor parte del tiempo se dedicaba a las funciones [[NvPutImage|NvPutImage]]() y [[NvWaitNotifier|NvWaitNotifier]](). El resto de funciones eran inapreciables. 

Cuando jb17some proporcionó código fuente anotado por oprofile (o sus herramientas), apareció la prueba definitiva: El controlador se encontraba ocupado esperando el "notificador de transferencia de DMA finalizada". 

Un par de horas más tarde ahuillet consiguió hacer funcionar su oprofile y confirmó algunos de los hallazgos de jb17some: la mayor parte del tiempo se dedicaba a esperar la notificación. Y dado que memcpy() no tenía que esperar al notificador en absoluto, era mucho más rápido que una DMA de PCI. Pero la DMA de PCI resultó ser más lenta que AGP, ya que se dedicaba más tiempo a esperar ocupados por la notificación de DMA. 

Volviendo a realizar nuevas pruebas con oprofile, se vió que ahora que en torno al 50% del tiempo se dedicaba a  copy_from_user() / copy_to_user(). Puesto que nouveau no debería entrar en el espacio del kernel demasiado a menudo, ni tampoco con "montones de datos", tal como dijo marcheu, estos resultados crearon extrañeza. El gráfico de llamadas de oprofile mostró que estaba relacionado con las funciones internas del servidor X. 

EDICIÓN: Este fue el resultado de una prueba incorrecta. Las funciones copy_*_user pueden ser evitadas usando memoria compartida. 

Entonces, como prueba rápida, se eliminó la espera y se llevaron a cabo más pruebas. El resultado fue: PCI era dos veces más rápido que antes, y AGP un 10% más. Todavía memcpy() era más rápido, y DMA de PCI producía una visualización incorrecta (tal como era de esperar, ya que se eliminó deliberadamete el punto de sincronización). Tal vez deberíamos indicar el hecho de que Xv todavía no está listo para el "usuario normal", ya que no es capaz de mostrar más de un video a la vez sin problemas de visualización... 

Toda esta confusión se puede resumir así: 

* en tarjetas NV50 y posteriores Xv todavía no funciona 
* EXA es acelerada con AGP o PCI DMA 
* los notificadores de transferencia de DMA y la espera que producen hacen el DMA lento 
Darktama tampoco estaba parado: integró las ramas nv50-branch y randr-1.2 en el git de DDX de modo que ya no necesita incluir los cambios de otras dos ramas (randr-1.2 y master). Por tanto, si quieres usar una tarjeta NV50 / G8x, por favor usa la rama randr-1.2, ya que la rama nv50-branch ya no se usa. 

Aparte de esa integración, Darktama añadió algunos parches a randr-1.2 que deberían mejorar el rendimiento de las NV50 / G8x para otros valores distintos a "greedy" de "[[MigrationHeuristic|MigrationHeuristic]]". 

hughsie intentó hacer funcionar la última versión de nouveau en FC7. No fue fácil ya que el DRM del kernel y el DRM en git están desincronizados en estos momentos. Sin embargo, lo logró y comprobó que funcionaba correctamente (según gtkperf) ([[http://hughsient.livejournal.com/29989.html|http://hughsient.livejournal.com/29989.html]]). Prometió incluir una versión más reciente en FC7 en cuanto el DRM se ajuste a la versión del kernel. 


### Ayuda necesaria

Nos gustaría que los poseedores de tarjetas 8800 probasen el driver actual y nos informasen de su resultado. Dado que solamente tenemos disponible dos tarjetas G84 para el desarrollo y pruebas, se agradece mucho la ayuda de los usuarios que tengan este hardware. Por favor: usad la rama randr-1.2 e informad a Darktama. 

Y necesitamos MMioTraces de NV41, NV42, NV44,NV45, NV47,NV48  y NV4C. Por favor, hazte notar en nuestro canal si puedes ayudar. 

Si no te importa, por favor prueba los parches de ahuillet en git://people.freedesktop.org/~ahuillet/xf86-video-nouveau e infórmale. Sin embargo, estate preparado para encontrarte con problemas, disfuncionalidades y bloqueos, ya que ¡este es un trabajo en marcha! 
