[[!table header="no" class="mointable" data="""
 [[Accueil|FrontPage-fr]]  |  [[TiNDC 2006|IrcChatLogs-fr]]  |  [[TiNDC 2007|IrcChatLogs-fr]]  |  [[TiNDC 2008|IrcChatLogs-fr]]  |  [[Archives anciennes|IrcChatLogs-fr]]  |  [[Archives récentes|IrcChatLogs-fr]]  | DE/[[EN|Nouveau_Companion_36]]/[[ES|Nouveau_Companion_36-es]]/[[FR|Nouveau_Companion_36-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Le compagnon irrégulier du développement de Nouveau (TiNDC)


## Édition du 7 mars 2008


### Introduction

Bienvenue à nouveau pour notre TiNDC. Juste pour s'assurer que le "i" dans le nom est vraiment pour "irrégulier", cette édition arrive un peu tard. La raison est simple : nous avions le FOSDEM à Bruxelles les 23/24 février. Puisque Marcheu a effectué une présentation là-bas et que quelques membres du projet étaient là-bas aussi, j'ai reporté cette édition afin d'avoir plus de retours. 

Pendant ce temps, [[la première partie|http://lwn.net/Articles/269558]] et [[la deuxième partie|http://lwn.net/Articles/270830]] de notre article "Status of Nouveau" est accessible sur [[lwn.net|http://lwn.net]] (ndt : une traduction française [["Un point sur le projet Nouveau"|http://linuxfr.org/2008/02/19/23727.html]] est disponible sur [[linuxfr.org|http://linuxfr.org]]). 

Vous savez ce que j'appelle sabotage? Je pense que le sabotage c'est quand IRC est absolument silentieux, je terme l'édition (alors) courante, l'envoie pour la publication et dans les heures qui suivent, à la fois Marcheu et Darktama commencent à envoyer leurs modifications sur Gallium sur dans le code principal (branche Gallium). Donc juste pour que vous le sachiez : j'ai officiellement la poisse ;-) 


### Statut actuel

Le travail sur la 3D a sérieusement commencé. Marcheu m'a déjà dit avant qu'il allait commencer le travail sur Gallium pour les cartes NV3x avant le FOSDEM et qu'il essaierait de le finir le plus possible, afin de passer la responsabilité de la 3D sur NV3x à d'autres membres du projets ayant les cartes qui vont bien. 

Après quelques jours, il a fait ce qu'il avait prévu et a commencé à remonter son travail sur le serveur. Pendant le weekend du FOSDEM, pmdata a reprit où marcheu avait arrêté et a envoyé ses deux premiers patchs. 

Donc, un peu négligé parce que j'ai raté ça deux fois dans les logs : Marcheu a travaillé sur une solution pour utiliser Gallium sur de vieilles cartes 2D. Il en reparlera un peu plus en détail dans notre section FOSDEM un peu plus bas. Il a déjà envoyé (comme noté ci-dessus) ses modification dans la branche Gallium. 

* ahuillet, pmdata travaillent sur NV30 (ahuillet commencera à la fin de ce mois-ci) 
* p0g souhaite travailler sur NV1x une fois que marcheu aura fini (p0g : bien sûr, si marcheu ne finit pas pas avant environ un an, je pourrais bien brancher ma nv30 ou nv40 et travailler là-dessus :-)) 
* Marcheu travaille sur un driver Gallium pour NV1x 
* Darktama travaille sur NV4x et un peu sur NV5x 
* Stillunknown et Malc0 travaillent sur le _mode setting_ (Randr1.2) 
Ceci étant dit, vous pouvez notez que notre driver DRI actuel (le lien est sur la page principale du Wiki) est obsolète et n'est plus supporté! 

Donc je voulais juste mentionner ça pour éviter de me faire accuser d'oublier des sujets importants :) 

Finallement, le problème avec les fonctions _Upload To Screen_/_Download From Screen_ sur PPC ont été résolus. Utiliser PCIgart marchera maintenant dans tous les cas. 

Comme déjà mentionné dans notre dernière édition, il y avait des problèmes avec les performances de l' _adapter_ de textures bi-cubique sur certaines cartes. Donc après avoir discuté avec marcheu, stillunknown a commencé à implémenter un second _adapter_ : 

* bi-linéaire (défaut) 
* bi-cubique (sélectionnable) 
Un autre sujet depuis quelques version est MMioTrace et son inclusion dans la branche officielle du noyau. Sur la LKML il a été suggéré de passer à un nouveau _framework_ du noyau : "ftrace". Il est principalement développé par Ingo Molnar et Steven Rostedt mais je ne connais pas de page web donnant plus de détails. La seule chose disponible est un _readme_ ici : [[http://people.redhat.com/mingo/sched-devel.git/README|http://people.redhat.com/mingo/sched-devel.git/README]] 

Ftrace offre toute sorte de fonctions de traces et permet à d'autres modules de s'y connecter, se construire et offre des fonctionnalités additionnelles. MMioTrace sera un de ses premiers utilisateurs. Pour l'instant les gars du noyau et pq (juste por MMioTrace) visent le noyau 2.6.26. 

La discussion à propos de la version actuelle de MMioTrace peut être trouvé ici sur la LKML : [[http://marc.info/?l=linux-kernel&m=120387271214433&w=2|http://marc.info/?l=linux-kernel&m=120387271214433&w=2]] 

Ok, maintenant l'assortiment habituel de sujets rapides : 

* benkai passe du temps à essayer de faire marcher le _suspend_/_resume_ sur Nouveau. Pour l'instant il est dans une phase de "_trial and error_" et il ne sait pas si il réussira ou pas. Il résume ses découvertes ici : [[http://nouveau.freedesktop.org/wiki/Suspend_support|http://nouveau.freedesktop.org/wiki/Suspend_support]] 
* Après que Marcheu a fini son travail initial sur NV30 il a commencé à travailler sur Gallium sur NV1x. Après avoir terminé un driver basique pour cette carte (par exemple sans le _texturing_) il veut maintenant acheter une carte NV5x et continuer là-dessus (Editeur : Yay!). 
* Malc0 a essayé de corriger quelques problèmes Randr1.2 sur PPC. Kelnos a testé ces patchs qui ont donné de bons résultats. Le seul problème restant était que Nouveau pensait que Kelnos avait un LVDS _dual link_. En désactivant ce bit de configuration, Nouveau a bien marché, même en mode texte. De l'aide additionnelle a été apportée par sbriglie et moondrake ce qui a conduit a des indices supplémentaires à propos de l'erreur et des rustines. 
* Nouveau est construit par le service tinderbox de freedesktop. Statut actuel : vert. [[http://tinderbox.freedesktop.org/|http://tinderbox.freedesktop.org/]] 
* Marcheu a fouillé son stock de cartes et a testé leur compatibilité avec Randr1.2. Il a trouvé quelques bugs avec NV4, NV11 et NV20 (bugs #14820 à 14825) dont quelques-uns ont été résolus dans les heures suivant leur soumission. 
Donc Randr1.2 marche maintenant sur la plupart des configurations et des cartes NVidia, avec juste quelques problèmes restant sur NV11 et NV17. En fait, ça marche tellement bien que Marcheu pousse Malc0 et Stillunknown à mettre Randr1.2 dans Nouveau par défaut. 

Le planning actuel est environ à un mois de changer les paramètres par défaut, et un mois supplémentaire pour des raisons de sauvegardes avant que le code ne soit supprimé. Peu de temps après cela, nous pourrions déplacer le code du _modesetting_ dans le noyau. Cependant, cela voudrait dire que nous aurions besoin que le DRM supporte cela plus TTM, qui est également requis pour le _modesetting_. 


### Aide requise

Comme toujours : regardez notre page [[TestersWanted|TestersWanted]]. Et puisque nous voulons démarrer le développement sur NV5x : contactez-nous si vous avez du matériel disponible et que vous souhaitez le donner (n'importe quel type de carte 8x00 et/ou de carte PCIe). Et testez également Randr1.2 avant qu'on le mette par défaut! 

Dans le cas où vous auriez besoin d'aide avec Nouveau, envoyez un mail sur la mailing-list ou créez une entrée sur le Bugzilla. Il y a peu de chance d'avoir de l'aide sur le channel IRC qui est réservé au développement. 


### Spécial : FOSDEM 2008

Ok, le dernier weekend de février il y avait le FOSDEM à Bruxelles. Malheureusement, je n'ai pas pu y participer mais nous avons quelques liens pour vous des présentations de Marcheu : les vidéos sont ici : [[http://radeonhd.org/?page=fosdem_videos|http://radeonhd.org/?page=fosdem_videos]] ou [[ftp://ftp.suse.de/private/bk/fosdem-2008/|ftp://ftp.suse.de/private/bk/fosdem-2008/]] . 2 est le plus bas encodage utilisé, 4 le meilleur. Les slides peuvent être trouvés ici : [[http://icps.u-strasbg.fr/~marchesin/nvdri/fosdem2008.pdf|http://icps.u-strasbg.fr/~marchesin/nvdri/fosdem2008.pdf]] 

Mais ce n'est pas tout, commençons à embêter Marcheu et les autres avec des questions à propos de la manière dont ça s'est passé et de leur point de vue. 

**TiNDC: ** 

* Quel était l'intérêt porté à la présentation et comment a-t'elle été reçue? 
**Marcheu** 

* Je pense que la présentation a été bien reçue. Nous avions un nombre conséquent de personne à y assister et ils étaient d'accord que manger des fruits était bon pour la santé. Manger des fruits et aller dans le sauna de la salle de développement X.Org sont deux activités qui permettent aux gens de rester en forme! 
**TiNDC:** 

* Dans ton discours tu as présenté une solution Gallium3D pour les anciennes cartes sans shaders. Pourrais-tu élaborer cette partie-là puisque j'ai totalement raté toutes les références à ça dans les logs. C'est pour ça aussi qu'il n'en a pas été fait mention dans le TiNDC jusqu'à maintenant.** ** 
**Marcheu** 

* C'est juste un peu de plomberie pour gérer les cartes à pipelines fixes à travers l'infrastructure Gallium. Il y a plus dans Gallium que la partie sur les pipelines programmables, donc je pense que ça vaut le coup de réutiliser le framework même dans le contexte de cartes à pipelines fixes. J'avais fait un premier test il y a quelques mois, mais j'avais écrit la partie Gallium en premier, ce qui était une erreur. J'ai réalisé un peu tard que ça n'avait aucun sens de faire de tels changements de nulle part, il faut tester ceux-ci sur un driver. J'appréhende maintenant le problème depuis l'autre côté, en faisant un simple driver Gallium NV10 d'abord (sans les textures par exemple). 
**TiNDC:** 

* Comment ça marche? Comment supporter les shaders sur les cartes qui n'en ont aucun? 
**Marcheu** 

* Pour le pipeline programmable, le _state tracker_ émet des instructions de shaders qui implémentent un état OpenGL donné. Pour les pipelines fixes, le _state tracker_ traduit le même état OpenGL en de nouvelles instructions, qui sont des shaders de fonctions fixes. Ces instructions représentent la fonctionnalité de pipeline fixe, et sont stockées de la même manière que les autres shaders, la partie interne de Gallium n'a donc pas à être modifiée. Seulement le frontend et le backend de Gallium doit être adapté pour gérer des instructions supplémentaires. C'est la manières la plus directe de gérer ça. 
**TiNDC:** 

* Donc qui travaille là-dessus maintenant? Quelle tâche sera abordée ensuite? 
**Marcheu** 

* Je veux vraiment continuer sur Gallium NV10 et NV30, donc je continuerais de travailler où mon aide sera requise. Après ça, je partirais sur NV50 comme tu l'as mentionné. 
**TiNDC:** 

* Maintenant, la pièce Xorg du FOSDEM n'est pas uniquement à propos de Nouveau, c'est également un endroit où les _hackers_ Xorg se rencontrent. Donc y a-t'il eu des choses intéressantes à tirer des discussions que tu as eu avec les autres projets Xorg? 
**Marcheu** 

* Oui, la chose la plus intéressante et d'apprendre des expériences des autres (des choses comme quelles architectures logicielles marchent ou ne marchent pas, quelles solutions sont rapides ou lentes...). Ça peut vous sauver beaucoup de temps. Je ne suis pas intéressé à essayer toutes les solutions possibles, puisque ça demande d'implémenter beaucoup de prototypes, ce qui est une perte de temps (par exemple avec le TTM ou les implémentation de _swap buffers_). J'ai aussi échangé des idées avec Michael Meeuwisse (le gars d'OpenVGA) sur comment on pourrait avoir des shaders compilés en FPGA en temps réel. Ça porrait sûrement permettre une exécution des shaders très rapide, et allouer dynamiquement autant de portes que possibles pour ce que la carte est en train de faire actuellement, quoi que ce soit. Je pense que c'est une manière qui permettrait aux cartes Open{VGA,Graphics} de se faire remarquer par rapport aux solutions graphiques existantes. 
**TiNDC: ** 

* Des rumeurs circulaient comme quoi tu ne serais pas le seul développeur Nouveau au FOSDEM. Donc de quoi avez-vous discuté? 
**Marcheu** 

* Nourriture, bière, et végétarisme (malc0 et ahuillet sont respectivement végétarien et semi-végétarien :). Plus important, j'ai pu rencontrer ahuillet, benkai, malc0 et p0g en personne, ce qui est très sympa (et j'espère qu'ils pensent la même chose à propos de moi ;-). Même si nous développons régulièrement sur le même bout de code, nous ne nous étions jamais rencontrés. Ce qui est bien avec ces évènements, c'est que nous pouvons nous rencontrer en personne et dicuter de choses non techniques. En ce qui concerne les conférences, je ne pense pas que le côté technique soit le plus important, puisque ça peut aussi se dérouler en ligne avant ou après l'évènement. Mais le côté social peut uniquement se faire lorsque vous vous rencontrez en personne. 
**TiNDC:** 

* Depuis décembre environ, nous avons discuté de sortir un driver "_2D only_" comme version 1.0. Êtes-vous arrivés à un concensus là-dessus? 
**Marcheu** 

* Il y a un consensus, que nous devons stabiliser les interfaces avant. Je pense que Randr1.2 par défaut et le déplacement du _modesetting_ dans le noyau vont arriver rapidement maintenant. TTM pourrait prendre un peu plus de temps, en fonction de la manière dont nous l'implémentons. 
**TiNDC:** 

* Phoronix était là également, as-tu rencontré Mr. Larabel? 
**Marcheu** 

* Yep, bien que nous n'avions malheureusement pas beaucoup de temps pour discutter beaucoup. Cette année, le FOSDEM a ressemblé un grand nombre de personnes sympa de X.Org, donc les discussions ne pouvaient pas être approfondies avec tous. J'espère que je pourrais assister au XDC afin que nous nous rencontrions à nouveau. 
**TiNDC:** 

* À propos de Phoronix : comment a été distribué le matériel offert? 
**Marcheu** 

* Ce n'était pas seulement du matériel de Phoronix, mais aussi des dons de particuliers. Bien que mon équipement de voyage habituel (portable + vêtements + brosse à dents) rentrait dans mon sac à dos, il y avait trop de matériel et j'ai fini par prendre un sac de voyage uniquement pour le matériel. Merci à tous les donateurs! De ce que je me souviens, malc0 a eu une NV4, NV11, NV20 et NV40. ahuillet a eu une NV30GL. Je ne savais pas que p0g venais avant d'être à Buxelles (il nous a pris quelque peu par surprise), donc je n'avais pas eu l'occasion de prendre quelque chose spécialement pour lui. Il me reste encore une carte mère + CPU et un CPU (de Phoronix), je vais sans doute diviser ça entre pmdata et ahuillet, mais pmdata doit encore y penser. Ou peut-être pourrons-nous faire un jeu où le premier qui parvient à faire marcher _gears_ récupèrera tout ;-) 
**TiNDC:** 

* Donc venez ici, chanceux, et dites-nous ce que vous allez faire avec tout ce matériel. À quoi va s'attaquer nouveau maintenant? 
**ahuillet:** 

* La NV30GL que j'ai récupéré est la carte graphique la plus puissante que j'ai jamais touchée. Sans rire, j'étais bloqué avec une NV28 jusqu'ici. Je vais l'utiliser pour comprendre les cartes à base de shaders - qui sont pour l'instant purement de la théorie pour moi - en travaillant sur la 3D. J'espère démarrer en avril. (Rédacteur : la réponse de Malc0 est un peu plus bas) 
**TiNDC:** 

* Faire une présentation sur "l'état de Nouveau" au FOSDEM semble être une bonne tradition maintenant, avec des présentations depuis la création du projet. Allez-vous garder cette tradition l'an prochain? Aux autres : avez-vous l'intention de revenir? 
**Marcheu** 

* Au FOSDEM 2006, il y avait juste 1 développeur de Nouveau (moi). C'était la première annonce, donc on ne pouvait pas espérer plus. Au FOSDEM 2007, deux développeurs étaient là (Koala et moi). Le FOSDEM 2008 a vu la venue de 4 devs (ahuillet, malc0, p0g et moi), donc je suppose que nous ne pouvons aller que exponentiellement à partir de là. Ça serait une tradition sympa, que pensez-vous de FOSDEM 2009 avec 8 développeurs Nouveau :) J'espère vraiment revenir au FOSDEM l'année prochaine. Le FOSDEM voit 4000 visiteurs chaque année, donc l'audience est supérieure à ce qu'il y a au XDC/XDS qui rassemble uniquement des développeurs X.Org. C'est certainement une bonne manière d'atteindre une plus grande audience. 
**Malc0** 

* Trop tôt pour le dire, mais j'aimerais y être, certainement. 
De plus, Malc0 a accepté de répondre à quelques autres questions à propos de l'état actuel de Randr1.2 : 

**TiNDC:** 

* Bonjour, tu as eu une certaine quantité de matériel par Marcheu donné par Phoronix et des particuliers, que vas-tu essayer de faire avec? Quels problèmes souhaites-tu de résoudre? 
**Malc0:** 

* À la base, je l'ai eu afin de pouvoir facilement tester mon code sur une plus grande variété de cartes (pour le démarrage à froid, le _modesetting_ noyau, le _suspend_/_resume_). Ahuillet m'a donné une NV11, je pensais qu'elle ne marchait pas et j'allais corriger le problème, mais on dirait que ça marche bien :) 
**TiNDC:** 

* Donc, que pense-tu de Randr1.2 dans Nouveau pour l'instant? Marcheu veut enlever l'ancien code on dirait :) Et si je comprends correctement, soit nous soit DRM en général va avoir le _mode setting_ dans le noyau dans les prochains 2-3 mois? 
**Malc0:** 

* Randr1.2 est plutôt bon maintenant. Il y a quelques bugs sur NV11/NV17 sur lesquels je travaille actuellement (nous ne pouvons évidemment pas enlever l'ancien code avant que l'affichage simple marche pour tout le monde). [Un mois après avoir mis Randr1.2 par défaut (ndt : la phrase n'a pas de fin ;))] Après quelques mois, le _modesetting_ noyau ne devrait pas être trop dûr; il suit de **très** près la structure de Randr1.2. J'attends quand même que les gars de Intel fassent le tri dans les problèmes avec le _modesetting_ noyau et le DDX :). Et bien sûr, nous aurons besoin de TTM... 
**TiNDC:** 

* Oui, si je comprends bien darktama, Nouveau [sur NV4x] sera à plus ou moins long terme gêné à cause des problèmes de mémoire tels que des pertes de mémoire ou une gestion de la mémoire simpliste... 
**Malc0:** 

* Il y a ça, et le fait que l'infrastructure _modesetting_ noyau semble supposer que l'on utilise l'API TTM. Il y a peut-être un moyen de contourner ça que darktama / marcheu pourrait truver, mais je pense que les bénéfices d'avoir un gestionnaire de mémoire propre fertait d'un tel effort une vision à trop court terme. (Rédacteur : cela n'est pas contredit par marcheu ou darktama) 
**TiNDC:** 

* Tu n'as rien d'intéressant à propos du FOSDEM que tu aimerais que j'écrive? 
**Malc0:** 

* Pas vraiment. Quelques présentations X.org étaient vraiment intéressantes (mais plutôt chaudes), et la bière et la socialisation étaient super, mais je vous laisse voir si vous voulez ça dans un TiNDC :) 
Rédacteur : au cas où vous vous demandez à propos des références à "sauna" et "chaudes", la salle X.org était chaude comme un four :) 

Allons-y et voyons quelques images : 

[[!img http://nouveau.freedesktop.org/wiki/Nouveau_Companion_36?action=AttachFile&do=get&target=p0g_and_malc0_small.jpg]    p0g (à gauche) and malc0 (à droite) ont trouvé un autre bug du BIOS? 

ahuillet (à gauche) et marcheu (à droite) pendant la présentation [[!img http://nouveau.freedesktop.org/wiki/Nouveau_Companion_36?action=AttachFile&do=get&target=ahuillet_and_marcheu_small.jpg] 

[[!img http://nouveau.freedesktop.org/wiki/Nouveau_Companion_36?action=AttachFile&do=get&target=openarena.jpg] 

"Oh ouah, NV4x marche vraiment - incroyable". Marcheu présentant [[OpenArena|OpenArena]] sur Nouveau (Marcheu : "Hé, c'est de la faute à daniel, pour avoir fait des blagues à propos de glxgears :)" ) 

Les images ont été prises par ahuillet, bbartek et Michael Larabel. Merci beaucoup. Et merci à Marcheu, Malc0 et ahuillet d'avoir répondu à mes questions. 

[[<<Édition précédente|Nouveau_Companion_35-fr]]  [[Édition suivante >>|Nouveau_Companion_37-fr]] 
