#!/usr/bin/perl

package IkiWiki::Plugin::fdo;

use warnings;
use strict;
use IkiWiki 3.00;
use HTML::Entities;
use URI::Escape;

sub import {
	hook(type => "getsetup", id => "fdo", call => \&getsetup);
	hook(type => "pagetemplate", id => "fdo", call => \&pagetemplate);
}

sub getsetup () {
	return
		plugin => {
			safe => 0,
			rebuild => undef,
			section => "rcs",
		},
		gitlab_edit_url => {
			type => "string",
			example => "https://gitlab.example.org/example/wiki/-/edit/master/[[file]]",
			description => "gitweb url to edit the file ([[file]] substituted)",
			safe => 1,
			rebuild => 1,
		},
		gitlab_src_url => {
			type => "string",
			example => "https://gitlab.example.org/example/wiki/-/blob/master/[[file]]",
			description => "gitweb url to view the file ([[file]] substituted)",
			safe => 1,
			rebuild => 1,
		},
}

sub pagetemplate (@) {
	my %params=@_;

	my $actions=$params{actions};
	my $page=$params{page};
	my $template=$params{template};

	if (defined $config{gitlab_edit_url} && length $config{gitlab_edit_url}) {
		my $u=$config{gitlab_edit_url};
		my $p=uri_escape_utf8($pagesources{$page}, '^A-Za-z0-9\-\._~/');
		$u=~s/\[\[file\]\]/$p/g;
		$template->param(editurl => $u);
		$actions++;
	}

	if (defined $config{gitlab_src_url} && length $config{gitlab_src_url}) {
		my $u=$config{gitlab_src_url};
		my $p=uri_escape_utf8($pagesources{$page}, '^A-Za-z0-9\-\._~/');
		$u=~s/\[\[file\]\]/$p/g;
		$template->param(getsourceurl => $u);
		$actions++;
	}
}

1
